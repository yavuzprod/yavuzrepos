import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:my_app/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Code Land",
      debugShowCheckedModeBanner: false,
      home: BottomNavigationBarMain(),
      theme: ThemeData(accentColor: Colors.white70),
      themeMode: ThemeMode.dark,
    );
  }
}

//DATA TABLE
class DataTableMain extends StatefulWidget {
  const DataTableMain({Key key}) : super(key: key);

  @override
  _DataTableExampleState createState() => _DataTableExampleState();
}

class _DataTableExampleState extends State<DataTableMain> {
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: PaginatedDataTable(
        header: Text('Nutrition'),
        rowsPerPage: _rowsPerPage,
        availableRowsPerPage: <int>[5, 10, 20],
        onRowsPerPageChanged: (int value) {
          setState(() {
            _rowsPerPage = value;
          });
        },
        columns: kTableColumns,
        source: DessertDataSource(),
      ),
    );
  }
}

////// Columns in table.
const kTableColumns = <DataColumn>[
  DataColumn(
    label: const Text('Dessert (100g serving)'),
  ),
  DataColumn(
    label: const Text('Calories'),
    tooltip: 'The total amount of food energy in the given serving size.',
    numeric: true,
  ),
  DataColumn(
    label: const Text('Fat (g)'),
    numeric: true,
  ),
  DataColumn(
    label: const Text('Carbs (g)'),
    numeric: true,
  ),
  DataColumn(
    label: const Text('Protein (g)'),
    numeric: true,
  ),
  DataColumn(
    label: const Text('Sodium (mg)'),
    numeric: true,
  ),
  DataColumn(
    label: const Text('Calcium (%)'),
    tooltip:
        'The amount of calcium as a percentage of the recommended daily amount.',
    numeric: true,
  ),
  DataColumn(
    label: const Text('Iron (%)'),
    numeric: true,
  ),
];

////// Data class.
class Dessert {
  Dessert(this.name, this.calories, this.fat, this.carbs, this.protein,
      this.sodium, this.calcium, this.iron);
  final String name;
  final int calories;
  final double fat;
  final int carbs;
  final double protein;
  final int sodium;
  final int calcium;
  final int iron;
  bool selected = false;
}

////// Data source class for obtaining row data for PaginatedDataTable.
class DessertDataSource extends DataTableSource {
  int _selectedCount = 0;
  final List<Dessert> _desserts = <Dessert>[
    new Dessert('Frozen yogurt', 159, 6.0, 24, 4.0, 87, 14, 1),
    new Dessert('Ice cream sandwich', 237, 9.0, 37, 4.3, 129, 8, 1),
    new Dessert('Eclair', 262, 16.0, 24, 6.0, 337, 6, 7),
    new Dessert('Cupcake', 305, 3.7, 67, 4.3, 413, 3, 8),
    new Dessert('Gingerbread', 356, 16.0, 49, 3.9, 327, 7, 16),
    new Dessert('Jelly bean', 375, 0.0, 94, 0.0, 50, 0, 0),
    new Dessert('Lollipop', 392, 0.2, 98, 0.0, 38, 0, 2),
    new Dessert('Honeycomb', 408, 3.2, 87, 6.5, 562, 0, 45),
    new Dessert('Donut', 452, 25.0, 51, 4.9, 326, 2, 22),
    new Dessert('KitKat', 518, 26.0, 65, 7.0, 54, 12, 6),
    new Dessert('Frozen yogurt with sugar', 168, 6.0, 26, 4.0, 87, 14, 1),
    new Dessert('Ice cream sandwich with sugar', 246, 9.0, 39, 4.3, 129, 8, 1),
    new Dessert('Eclair with sugar', 271, 16.0, 26, 6.0, 337, 6, 7),
    new Dessert('Cupcake with sugar', 314, 3.7, 69, 4.3, 413, 3, 8),
    new Dessert('Gingerbread with sugar', 345, 16.0, 51, 3.9, 327, 7, 16),
    new Dessert('Jelly bean with sugar', 364, 0.0, 96, 0.0, 50, 0, 0),
    new Dessert('Lollipop with sugar', 401, 0.2, 100, 0.0, 38, 0, 2),
  ];

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _desserts.length) return null;
    final Dessert dessert = _desserts[index];
    return DataRow.byIndex(
        index: index,
        selected: dessert.selected,
        onSelectChanged: (bool value) {
          if (dessert.selected != value) {
            _selectedCount += value ? 1 : -1;
            assert(_selectedCount >= 0);
            dessert.selected = value;
            notifyListeners();
          }
        },
        cells: <DataCell>[
          DataCell(Text('${dessert.name}')),
          DataCell(Text('${dessert.calories}')),
          DataCell(Text('${dessert.fat.toStringAsFixed(1)}')),
          DataCell(Text('${dessert.carbs}')),
          DataCell(Text('${dessert.protein.toStringAsFixed(1)}')),
          DataCell(Text('${dessert.sodium}')),
          DataCell(Text('${dessert.calcium}%')),
          DataCell(Text('${dessert.iron}%')),
        ]);
  }

  @override
  int get rowCount => _desserts.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

// BOTTOM NAVIGATION BAR
class BottomNavigationBarMain extends StatefulWidget {
  const BottomNavigationBarMain({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<BottomNavigationBarMain> {
  SharedPreferences sharedPreferences;
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    }
  }

  int _currentTabIndex = 0;
  String selectedFirmId = "1";
  Firm selectedFirm = null;
  GlobalKey keym = new GlobalKey<AutoCompleteTextFieldState<Firm>>();
  List<Firm> suggestions = [
    new Firm(1, "Minamishima"),
    new Firm(15, "The Meat & Wine Co Hawthorn East"),
    new Firm(34, "Florentino"),
    new Firm(43, "Syracuse Restaurant & Winebar Melbourne CBD"),
    new Firm(11, "Geppetto Trattoria"),
    new Firm(34, "Cumulus Inc."),
    new Firm(22, "Chin Chin"),
    new Firm(50, "Anchovy"),
    new Firm(47, "Sezar Restaurant"),
    new Firm(26, "Tipo 00"),
    new Firm(34, "Coda"),
    new Firm(11, "Pastuso"),
    new Firm(2, "San Telmo"),
    new Firm(36, "Supernormal"),
    new Firm(44, "EZARD"),
    new Firm(21, "Maha"),
    new Firm(42, "MoVida")
  ];
  @override
  Widget build(BuildContext context) {
    final _kTabPages = <Widget>[
      ListView(
        children: [
          Row(
            children: [
              DropdownButton(
                  value: selectedFirmId == null ? null : selectedFirmId,
                  items: [
                    DropdownMenuItem(
                      child: Text("First Item"),
                      value: "1",
                    ),
                    DropdownMenuItem(
                      child: Text("Second Item"),
                      value: "2",
                    ),
                    DropdownMenuItem(child: Text("Third Item"), value: "3"),
                    DropdownMenuItem(child: Text("Fourth Item"), value: "4")
                  ],
                  hint: Text('Şirket seçiniz'),
                  onChanged: (value) {
                    setState(() {
                      selectedFirmId = value;
                    });
                  }),
              FlatButton(
                onPressed: () {
                  Future<void> _showMyDialog(firmId) async {
                    return showDialog<void>(
                      context: context,
                      barrierDismissible: false, // user must tap button!
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('AlertDialog Title'),
                          content: SingleChildScrollView(
                            child: ListBody(
                              children: <Widget>[
                                Text('This is a demo alert dialog.'),
                                Text(firmId),
                              ],
                            ),
                          ),
                          actions: <Widget>[
                            TextButton(
                              child: Text('Approve'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }

                  _showMyDialog(selectedFirmId);
                },
                child: Text(
                  "Flat Button",
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
          ),
          MyDataTable()
        ],
      ),
      ListView(
        children: [DataTableMain()],
      ),
      //DataTableMain(),
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(icon: Icon(Icons.cloud), label: 'Ziyaret'),
      BottomNavigationBarItem(icon: Icon(Icons.alarm), label: 'Stok'),
    ];
    assert(_kTabPages.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(_kBottmonNavBarItems[_currentTabIndex].label,
            style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              sharedPreferences.clear();
              sharedPreferences.commit();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                  (Route<dynamic> route) => false);
            },
            child: Text("Çıkış", style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}

// DROPDOWN MENU
class DropdownMain extends StatefulWidget {
  @override
  _DropdownState createState() {
    return _DropdownState();
  }
}

class _DropdownState extends State<DropdownMain> {
  String _value;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: DropdownButton<String>(
        items: [
          DropdownMenuItem<String>(
            child: Text('Item 1'),
            value: 'one',
          ),
          DropdownMenuItem<String>(
            child: Text('Item 2'),
            value: 'two',
          ),
          DropdownMenuItem<String>(
            child: Text('Item 3'),
            value: 'three',
          ),
        ],
        onChanged: (String value) {
          setState(() {
            _value = value;
          });
        },
        hint: Text('Select Item'),
        value: _value,
      ),
    );
  }
}

class Firm {
  num id;
  String name;

  Firm(this.id, this.name);
}

class ArbitrarySuggestionType {
  //For the mock data type we will use review (perhaps this could represent a restaurant);
  num stars;
  String name, imgURL;

  ArbitrarySuggestionType(this.stars, this.name, this.imgURL);
}

class SecondPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  List<ArbitrarySuggestionType> suggestions = [
    new ArbitrarySuggestionType(4.7, "Minamishima",
        "https://media-cdn.tripadvisor.com/media/photo-p/0f/25/de/0c/photo1jpg.jpg"),
    new ArbitrarySuggestionType(1.5, "The Meat & Wine Co Hawthorn East",
        "https://media-cdn.tripadvisor.com/media/photo-s/12/ba/7d/4c/confit-cod-chorizo-red.jpg"),
    new ArbitrarySuggestionType(3.4, "Florentino",
        "https://media-cdn.tripadvisor.com/media/photo-s/12/fc/bb/11/from-the-street.jpg"),
    new ArbitrarySuggestionType(
        4.3,
        "Syracuse Restaurant & Winebar Melbourne CBD",
        "https://media-cdn.tripadvisor.com/media/photo-p/07/ad/76/b0/the-gyoza-had-a-nice.jpg"),
    new ArbitrarySuggestionType(1.1, "Geppetto Trattoria",
        "https://media-cdn.tripadvisor.com/media/photo-s/0c/85/3d/cb/photo1jpg.jpg"),
    new ArbitrarySuggestionType(3.4, "Cumulus Inc.",
        "https://media-cdn.tripadvisor.com/media/photo-s/0e/21/a0/be/photo0jpg.jpg"),
    new ArbitrarySuggestionType(2.2, "Chin Chin",
        "https://media-cdn.tripadvisor.com/media/photo-s/0e/83/ec/07/triple-beef-triple-bacon.jpg"),
    new ArbitrarySuggestionType(5.0, "Anchovy",
        "https://media-cdn.tripadvisor.com/media/photo-s/07/e7/f6/8e/daneli-s-kosher-deli.jpg"),
    new ArbitrarySuggestionType(4.7, "Sezar Restaurant",
        "https://media-cdn.tripadvisor.com/media/photo-s/04/b8/23/d1/nevsky-russian-restaurant.jpg"),
    new ArbitrarySuggestionType(2.6, "Tipo 00",
        "https://media-cdn.tripadvisor.com/media/photo-s/11/17/67/8c/front-seats.jpg"),
    new ArbitrarySuggestionType(3.4, "Coda",
        "https://media-cdn.tripadvisor.com/media/photo-s/0d/b1/6a/84/photo0jpg.jpg"),
    new ArbitrarySuggestionType(1.1, "Pastuso",
        "https://media-cdn.tripadvisor.com/media/photo-w/0a/d9/cf/52/photo4jpg.jpg"),
    new ArbitrarySuggestionType(0.2, "San Telmo",
        "https://media-cdn.tripadvisor.com/media/photo-s/0e/51/35/35/tempura-sashimi-combo.jpg"),
    new ArbitrarySuggestionType(3.6, "Supernormal",
        "https://media-cdn.tripadvisor.com/media/photo-s/0e/bc/63/69/mr-miyagi.jpg"),
    new ArbitrarySuggestionType(4.4, "EZARD",
        "https://media-cdn.tripadvisor.com/media/photo-p/09/f2/83/15/photo0jpg.jpg"),
    new ArbitrarySuggestionType(2.1, "Maha",
        "https://media-cdn.tripadvisor.com/media/photo-s/10/f8/9e/af/20171013-205729-largejpg.jpg"),
    new ArbitrarySuggestionType(4.2, "MoVida",
        "https://media-cdn.tripadvisor.com/media/photo-s/0e/1f/55/79/and-here-we-go.jpg")
  ];

  GlobalKey key =
      new GlobalKey<AutoCompleteTextFieldState<ArbitrarySuggestionType>>();

  AutoCompleteTextField<ArbitrarySuggestionType> textField;

  ArbitrarySuggestionType selected;

  _SecondPageState() {
    textField = new AutoCompleteTextField<ArbitrarySuggestionType>(
      decoration: new InputDecoration(
          hintText: "Search Resturant:", suffixIcon: new Icon(Icons.search)),
      itemSubmitted: (item) => setState(() => selected = item),
      key: key,
      suggestions: suggestions,
      itemBuilder: (context, suggestion) => new Padding(
          child: new ListTile(
              title: new Text(suggestion.name),
              trailing: new Text("Stars: ${suggestion.stars}")),
          padding: EdgeInsets.all(8.0)),
      itemSorter: (a, b) => a.stars == b.stars
          ? 0
          : a.stars > b.stars
              ? -1
              : 1,
      itemFilter: (suggestion, input) =>
          suggestion.name.toLowerCase().startsWith(input.toLowerCase()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text('AutoComplete TextField Demo Complex'),
      ),
      body: new Column(children: [
        new Padding(
            child: new Container(child: textField),
            padding: EdgeInsets.all(16.0)),
        new Padding(
            padding: EdgeInsets.fromLTRB(0.0, 64.0, 0.0, 0.0),
            child: new Card(
                child: selected != null
                    ? new Column(children: [
                        new ListTile(
                            title: new Text(selected.name),
                            trailing: new Text("Rating: ${selected.stars}/5")),
                        new Container(
                            child: new Image(
                                image: new NetworkImage(selected.imgURL)),
                            width: 400.0,
                            height: 300.0)
                      ])
                    : new Icon(Icons.cancel))),
      ]),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyDataTable extends StatelessWidget {
  MyDataTable({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Name',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Age',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'Role',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Sarah')),
            DataCell(Text('19')),
            DataCell(Text('Student')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('Janine')),
            DataCell(Text('43')),
            DataCell(Text('Professor')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('William')),
            DataCell(Text('27')),
            DataCell(Text('Associate Professor')),
          ],
        ),
      ],
    );
  }
}
